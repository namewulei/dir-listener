package com.htht;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MakeFileTest {
	public static void main(String[] args) {
		String filePath1 = "D:\\maven-pie\\test1";
		String filePath2 = "D:\\maven-pie\\test2";
		while(true){
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmsss");
			String logName = format.format(new Date())+".log";
			
			File logFile1 = new File(filePath1 + File.separator + logName);
			try {
				logFile1.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			File logFile2 = new File(filePath2 + File.separator + logName);
			try {
				logFile2.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
