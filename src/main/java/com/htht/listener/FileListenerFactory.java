package com.htht.listener;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.HiddenFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

public class FileListenerFactory {

	// 设置轮询间隔
	private final long interval = TimeUnit.SECONDS.toMillis(1);

	public FileAlterationMonitor getMonitor(String monitorDir, String fileType, String regex) {
		// 创建过滤器
		IOFileFilter directories = FileFilterUtils.and(FileFilterUtils.directoryFileFilter(), HiddenFileFilter.VISIBLE);
		IOFileFilter files = FileFilterUtils.and(FileFilterUtils.fileFileFilter());
		IOFileFilter filter = FileFilterUtils.or(directories, files);

		// 装配过滤器
		FileAlterationObserver observer = new FileAlterationObserver(new File(monitorDir), filter);

		// 向监听者添加监听器，并注入业务服务
		observer.addListener(new FileListener(fileType, regex));

		// 返回监听者
		return new FileAlterationMonitor(interval, observer);
	}
}
