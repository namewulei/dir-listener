package com.htht.listener;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.htht.entity.ActionEnum;
import com.htht.entity.FileBean;
import com.htht.quartz.MonitorJob;
import com.htht.utils.MD5Util;
import com.htht.utils.YamlUtil;

public class FileListener extends FileAlterationListenerAdaptor{

	private static Logger log = Logger.getLogger(MonitorJob.class);
	private SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	private Map<String,FileBean> tempMap = new HashMap<>();
	private String logPath = YamlUtil.getString("logpath");
	private String fileType;
	private String regex;
	
    public FileListener(String fileType,String regex) {
		super();
		this.fileType = fileType;
		this.regex = regex;
	}

    private void addTempMap(File file, ActionEnum actionEnum) {
    	String fullFileName = file.getAbsolutePath();
		fullFileName = MD5Util.toMD5(fullFileName);
		FileBean filebean = new FileBean(actionEnum,fileType,file.getName(),file.getAbsolutePath(), file.lastModified(), file.length());
		tempMap.put(fullFileName, filebean);
    }
    
    /**
     * 文件创建
     */
    @Override
    public void onFileCreate(final File file) {
    	if(file.getName().matches(regex)) {
    		addTempMap(file, ActionEnum.A);
    	}
    }

    /**
     * 文件修改
     */
    @Override
    public void onFileChange(final File file) {
    	if(file.getName().matches(regex)) {
    		addTempMap(file, ActionEnum.U);
    	}
    }

    /**
     * 文件删除
     */
    @Override
    public void onFileDelete(final File file) {
    	if(file.getName().matches(regex)) {
    		addTempMap(file, ActionEnum.D);
    	}
    }

    /**
     * 监控开始
     */
    @Override
    public void onStart(final FileAlterationObserver observer) {
    	String logtxt = format.format(new Date())+".log";
    	String logFile = logPath + File.separator + logtxt;
    	File log = new File(logFile);
    	if(!log.exists()) {
    		try {
				log.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    	super.onStart(observer);
    }
    
    /**
     * 监控结束
     */
    @SuppressWarnings("static-access")
	@Override
    public void onStop(final FileAlterationObserver observer) {
    	log.info("-----------"+Thread.currentThread().getName()+","+tempMap.size()+"-----------");
    	FileWriter fileWritter = null;
    	BufferedWriter bw = null;
    	try {
    		Map<String,FileBean> newMap = new HashMap<>();
    		newMap.putAll(tempMap);
    		String logtxt = format.format(new Date())+".log";
    		String logFile = logPath + File.separator + logtxt;
    		File log =new File(logFile);
    		fileWritter = new FileWriter(log.getAbsolutePath(),true);
    		bw = new BufferedWriter(fileWritter); 
    		for(Map.Entry<String, FileBean> entry : newMap.entrySet()){
    			FileBean fileBean = entry.getValue();
    			Long now = System.currentTimeMillis();
    			Long time = fileBean.getModifyTime();
    			if(now - time > 1000) {
    				String fileBeanJson = JSONObject.toJSONString(fileBean);
    				this.log.info(fileBeanJson);
    				bw.write(fileBeanJson);
    				bw.newLine();
    				tempMap.remove(entry.getKey());
    			}
    		}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(bw!=null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fileWritter!=null) {
				try {
					fileWritter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
    	super.onStop(observer);
    }
}
