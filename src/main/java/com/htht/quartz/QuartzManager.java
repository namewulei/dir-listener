package com.htht.quartz;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzManager {
	private static String JOB_NAME = "DIR_LISTENER";
    
	public static void addJob(Class<MonitorJob> cls, String cron) {
        try {
        	SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        	Scheduler scheduler = schedulerFactory.getScheduler();
        	//创建任务
        	JobDetail jobDetail = JobBuilder.newJob(cls).withIdentity(JOB_NAME).build();
        	//执行规则
        	CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(JOB_NAME)
        			.withSchedule(CronScheduleBuilder.cronSchedule(cron)).build();
            // 触发器时间设定
            scheduler.scheduleJob(jobDetail, trigger);
            // 启动
            if (!scheduler.isShutdown()) {
            	scheduler.start();
            }
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        QuartzManager.addJob(MonitorJob.class,"* * * * * ?");
    }
}
