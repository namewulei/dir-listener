package com.htht.quartz;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.htht.listener.FileListenerFactory;
import com.htht.utils.YamlUtil;

public class MonitorJob implements Job{

	//记录启动的监控任务
	private static final List<FileAlterationMonitor> monitorList = new ArrayList<>();
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
	private static Logger log = Logger.getLogger(MonitorJob.class);
	
	@Override
	public void execute(JobExecutionContext arg) {
		try {
			log.info(format.format(new Date())+":重置任务");
			//清空旧任务
			if(monitorList.size()>0) {
				for(FileAlterationMonitor monitor : monitorList) {
					monitor.stop();
				}
				monitorList.clear();
			}
			
			//配置监控参数
			List<Map<String,Object>> configList = YamlUtil.getList("dir_array");
			for(Map<String,Object> map : configList) {
				String type = map.get("type").toString();
				String par_path = map.get("path").toString();
				String regex = map.get("regex").toString();
				//目录规则
				List<String> dirList = new ArrayList<>();
				//如果子目录规则存在，则遍历子目录
				if(map.get("subdir")!=null && map.get("old_dir_num")!=null) {
					String subdir = map.get("subdir").toString(); 
					int old_dir_num = Integer.parseInt(map.get("old_dir_num").toString());
					SimpleDateFormat dirFormat = new SimpleDateFormat(subdir);
					for(int i=0; i<=old_dir_num; i++) {
						Calendar calendar = Calendar.getInstance();
						calendar.add(Calendar.DATE, -i);
						String subpath = par_path + File.separator + dirFormat.format(calendar.getTime());
						if(!new File(subpath).exists()) {
							log.info("目录["+subpath+"]不存在");
							continue;
						}
						dirList.add(subpath);
					}
				}else {//子目录规则不存在，处理父目录
					if(!new File(par_path).exists()) {
						log.info("目录["+par_path+"]不存在");
						continue;
					}
					dirList.add(par_path);
				}
				//创建任务
				if(dirList.size()>0) {
					for(String path : dirList) {
						makeListener(type, path, regex);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param type 文件标志
	 * @param path 文件目录
	 * @param regex 过滤规则
	 */
	public static void makeListener(String type, String path, String regex) {
		//创建监听者
		FileListenerFactory fileListenerFactory = new FileListenerFactory();
        FileAlterationMonitor fileAlterationMonitor = fileListenerFactory.getMonitor(path,type,regex);
        try {
            fileAlterationMonitor.start();
            monitorList.add(fileAlterationMonitor);
            log.info("创建["+path+"]目录监听任务");
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
}
