package com.htht.entity;

public class FileBean {

	private ActionEnum action;	//动作
	private String fileType;	//文件类别
	private String fileName;	//文件名称
	private String filePath;	//完整路径
	private Long modifyTime;	//修改时间
	private Long size;			//文件大小
	
	public FileBean() {
		super();
	}
	public FileBean(ActionEnum action,String fileType, String fileName, String filePath, Long modifyTime, Long size) {
		super();
		this.action = action;
		this.fileType = fileType;
		this.fileName = fileName;
		this.filePath = filePath;
		this.modifyTime = modifyTime;
		this.size = size;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public ActionEnum getAction() {
		return action;
	}
	public void setAction(ActionEnum action) {
		this.action = action;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public Long getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(Long modifyTime) {
		this.modifyTime = modifyTime;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	@Override
	public String toString() {
		return "FileBean [action=" + action + ", fileType=" + fileType + ", fileName=" + fileName + ", filePath="
				+ filePath + ", modifyTime=" + modifyTime + ", size=" + size + "]";
	}
}
