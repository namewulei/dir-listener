package com.htht;

import java.io.File;

import com.htht.quartz.MonitorJob;
import com.htht.quartz.QuartzManager;
import com.htht.utils.YamlUtil;

/**
 * 目录监控
 */
public class MainApp {
	
	public static void main(String[] args) {
		//配置文件
		String configPath =  System.getProperty("user.dir");
		configPath += File.separator + "dir-listener-config.yml";
		YamlUtil.initConfig(configPath);
		//创建日志目录
		makeLogDir();
		//创建任务
		QuartzManager.addJob(MonitorJob.class, YamlUtil.getString("reset_task"));
	}
	
	/**
	 * 创建日志输出目录
	 */
	public static void makeLogDir() {
		//创建日志输出路径
		String logpath = YamlUtil.getString("logpath");
		File logDir = new File(logpath);
		if(!logDir.exists()) {
			logDir.mkdirs();
		}
	}
}
