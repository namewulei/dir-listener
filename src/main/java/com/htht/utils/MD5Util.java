package com.htht.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
	
	public static String toMD5(String context) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		md.update(context.getBytes());// 计算md5函数
        String hashedCode = new BigInteger(1, md.digest()).toString(16);// 16是表示转换为16进制数
        return hashedCode.toUpperCase();
	}
}
