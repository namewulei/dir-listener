package com.htht.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.yaml.snakeyaml.Yaml;

import com.htht.quartz.MonitorJob;

public class YamlUtil {
	
	private static Map<String,Object> map;
	private static Logger log = Logger.getLogger(MonitorJob.class);

	/**
	 * 初始化配置
	 * @param configPath
	 */
	@SuppressWarnings("unchecked")
	public static void initConfig(String configPath) {
		Yaml yaml = new Yaml();
		File configFile = new File(configPath);
		InputStream in = null;
		try {
			in = new FileInputStream(configFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if(in != null) {
			map = yaml.loadAs(in, Map.class);
			log.info(map);
		}else {
			log.error("配置文件["+configPath+"]不存在");
		}
	}
	
	/**
	 * 一级字符
	 * @param key
	 */
	public static String getString(String key) {
		if(map.get(key)!=null) {
			return map.get(key).toString();
		}
		log.error("配置项["+key+"]不存在");
		return null;
	}
	
	/**
	 * 一级数组
	 * @param key
	 */
	@SuppressWarnings("unchecked")
	public static List<Map<String, Object>> getList(String key) {
		if(map.get(key)!=null) {
			return (ArrayList<Map<String, Object>>) map.get(key);
		}
		log.error("配置项["+key+"]不存在");
		return new ArrayList<>();
	}
	
}
