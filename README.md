# dir-listener

#### 介绍
目录监控插件，基于org.apache.commons.io.monitor开发。使用yaml进行日志输出及任务配置，支持多目录监控配置。插件将监控到的内容以JSON格式写入指定文件，监控内容包括文件路径、文件名称、文件大小、修改时间。

#### 软件架构
Java,Log4j,Quartz


#### 使用说明

1.  使用maven生产可执行jar
2.  配置dir-listener-config.yml，并将文件放到可执行jar同级目录
3.  使用java -jar执行jar包